/*
ACTIVITY: 

	>> Create a new set of pokemon for pokemon battle.

	>> Solve the health of the pokemon that when tackle is invoked, current value of target’s health should decrease continuously as many times the tackle is invoked
	(target health - this attack)
	
	>> If health is below 10, invoke faint function

*/

function Pokemon(name, lvl, hp){

	// Properties
	this.name = name;
	this.lvl = lvl;
	this.health = hp * 2;
	this.attack = lvl;

	// methods
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);

		target.health -= this.attack;
		console.log(`${target.name}'s health is now reduced to ${target.health}`);
		if (target.health < 10) target.faint();
	};
	this.faint = function(){
		console.log(`${this.name} fainted`)
	}
};

let pikachu = new Pokemon("Pikachu", 3, 50);
let ratata = new Pokemon("Ratata", 5, 25);

for (let round = 0; round < 14; round++) {
	ratata.tackle(pikachu);
	pikachu.tackle(ratata);
}
